$(function () {
    function populateSelectPersonnes() {
        var file = "bdd/participants.txt";

        function traiterParticipant(nom) {
            // ajout du nom au select du modal
            $('#checkpersonnes').append('<input class="checkpersonnes" type="checkbox" id="' + nom + '" name="personne" value="' + nom + '"/>');
            // label
            $('#checkpersonnes').append('<label class="form-check-label" for="' + nom + '"> ' + nom + '</label><br>');
        }

        $.get(file, function (data) {
            var res = data.split("\n");
            for (var i = 0, len = res.length; i < len; i++) {
                traiterParticipant(res[i]);
            }
        })
    }

    populateSelectPersonnes();

});

function majHistorique() {
    var botw = {};
    if(document.getElementById("nombar").value == ""){
        return;
    }
    if(document.getElementById("date").value == ""){
        return;
    }
    if(document.getElementById("mdp").value != "le wifi"){
        return;
    }

    botw["nombar"] = document.getElementById("nombar").value;
    botw["date"] = document.getElementById("date").value;

    var personnes = "";
    var isFirst = true;
    var inputElements = document.getElementsByClassName('checkpersonnes');
    var comptage = 0;
    for (var i = 0; inputElements[i]; ++i) {
        if (inputElements[i].checked) {
            if (isFirst) {
                isFirst = false;
            } else {
                personnes += ",";
            }
            comptage++;
            personnes += inputElements[i].value;
        }
    }
    if(comptage = 0){
        return;
    }
    botw["personnes"] = personnes;

    function addBOTW(botw) {
            $.ajax({
                type: "POST",
                url: "/send/addbotw",
                data: botw
            });
            $('#ajoutBOTWModal').modal('toggle');
    }

    addBOTW(botw);

}