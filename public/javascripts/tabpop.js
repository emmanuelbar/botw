function remplirHistorique() {
    var file = "bdd/historique.json";
    $.getJSON(file, function (data) {


        function compareParDate(a,b) {
            if (new Date(a.date) < new Date(b.date))
                return 1;
            if (new Date(a.date) >new Date(b.date))
                return -1;
            return 0;
        }

        $.each(data.sort(compareParDate),function (i,val) {
            function traitLigneHisto(val) {
                $('#tabhisto tbody').append('<tr>');
                $('#tabhisto tbody').append('<td scope="row">' + val.date.replace(/T.*/,'') + '</td>'+'<td>' + val.nombar + '</td>'+'<td>' + val.personnes + '</td>');
                $('#tabhisto tbody').append('</tr>');
            }

            traitLigneHisto(val);
        });
    });
}

$(function () {
    remplirTableau();
    remplirHistorique();
});


function traitLigne(ligne) {

    // on ajoute une ligne au tableau
    $('#tableau tbody').append('<tr>');
        $('#tableau tbody').append('<td scope="row">' + ligne.nom + '</td>'+'<td>' + ligne.nbParticipation + '</td>'+'<td>' + ligne.date + '</td>');
    $('#tableau tbody').append('</tr>');
}

function remplirTableau() {




    var file = "bdd/historique.json";
    $.getJSON(file, function (data) {

        // pour chaque participant
        var file = "bdd/participants.txt";

        function traiterParticipant(nom) {
            var participant = {};
            participant.nom = nom;
            var comptage=0;
            // chaque BOTW
            var maxDate = "1900-01-01";
            $.each(data,function(i,val){
                if(val.personnes.includes(nom)){
                    comptage++;
                    if(val.date > maxDate){
                        participant.date = val.date;
                    }
                }

            });
            participant.nbParticipation = comptage;
            return participant;
            }

        $.get(file, function (data) {
            var res = data.split("\n");
            var participants = [];
            for (var i = 0, len = res.length; i < len; i++) {
                participants.push(traiterParticipant(res[i]));
            }

            function compare(a,b) {
                if (a.nbParticipation < b.nbParticipation)
                    return 1;
                if (a.nbParticipation > b.nbParticipation)
                    return -1;
                return 0;
            }


            $.each(participants.sort(compare),function (i,val) {
                traitLigne(val);
            });

        })

    })
}