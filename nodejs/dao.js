const pg = require('pg');
const PropertiesReader = require('properties-reader');
const fs = require('fs');
const jsonfile =require('jsonfile');

var env = process.env.NODE_ENV|| 'local';
// chargement des properties de bdd
var properties = PropertiesReader('./config/db.properties');
var host = properties.get(env+".db.host");
var user = properties.get(env+".db.user");
var password = properties.get(env+".db.password");
var database = properties.get(env+".db.database");
var port = properties.get(env+".db.port");


const config = {
    host: host,
    user: user,
    password: password,
    database: database,
    port: port,
    ssl: false
};
console.log(config);

const client = new pg.Client(config);

client.connect(err => {
    if (err) throw err;

});

function initdb() {
    var query = 'create\n' +
        '\tschema if not exists botw;\n' +
        '\n' +
        'create\n' +
        '\ttable if not exists\n' +
        '\t\tbotw.historique(\n' +
        '\t\t\tid serial primary key,\n' +
        '\t\t\tnombar text not null,\n' +
        '\t\t\tdatebar date not null,\n' +
        '\t\t\tpersonnes text not null\n' +
        '\t\t);\n' +
        '\n' +
        'create\n' +
        '\ttable if not exists\n' +
        '\t\tbotw.participants(\n' +
        '\t\t\tnom text unique not null\n' +
        '\t\t);';

    client
        .query(query)
        .then(() => {
        console.log('db init success !');
})
.catch(err => console.log(err))
.then(() => {
        console.log('init dao end');

});
}

var getListParticipant = function(){
    client.query('SELECT * FROM botw.participants', function(err, result) {

        function writeParticipants(participants) {
            var listParticipant ="";
            var isFirst = true;
            participants.forEach(function (value) {
                if(isFirst){
                    isFirst = false;
                    listParticipant+= value.nom;
                } else {
                    listParticipant += "\n"+value.nom;
                }
            })
            console.log(listParticipant);
            fs.writeFile("./public/bdd/participants.txt", listParticipant,function(err){
                if(err) {
                    return console.log(err);
                }

                console.log("Liste des participants chargée dans le fichier!");
            })
        }

        writeParticipants(result.rows);
    });
}

function insertHistorique(nombar,date,personnes){
    var nombarclean = nombar.replace('\'','\'\'');
    var values = 'values (DEFAULT,\''+nombarclean+'\',\''+date+'\',\''+personnes+'\')';
    console.log(insertHistorique);
    client.query('insert into botw.historique '+values, function(err, result) {
        if(err) {
            return console.log(err);
        } else {
            updateHistorique();
        }
    })
}

function updateHistorique(){
    console.log(updateHistorique);
    client.query('select * from botw.historique',function(err,result){
        console.log(result.rows);
        function writeHistoriqueTofile(historique) {
            console.log(writeHistoriqueTofile);
            var array = [];
            var i = 0;

            historique.forEach(function (value, index, pousser){
                function pousser(botw, voir){

                   array.push(botw);
                }
               var botw = {
                   nombar: value.nombar,
                   date: value.datebar,
                   personnes: value.personnes
               }
              pousser(botw);
            })
            write(array);
            function write(array){
                console.log(write);
                jsonfile.writeFile('./public/bdd/historique.json',array,function(err){
                    if(err){
                        console.log(err);
                    }
                })
            }
        }

        if(err){
            return console.log(err);
        } else {
            writeHistoriqueTofile(result.rows);
        }
    })
}



exports.initdb = initdb;
exports.getListParticipant = getListParticipant;
exports.insertHistorique=insertHistorique;